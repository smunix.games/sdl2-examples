# SDL 2 Examples

This project contains SDL 2 examples written in Haskell.

The code is based on the examples of the [SDL 2 library][sdl2-library].

## Requirements

The following packages need to be installed in your operating system:

- `sdl2`
- `sdl2-image`
- `sdl2-mixer`
- `sdl2-ttf`
- `cabal-install`

## Build

Update cabal package list:

```
cabal update
```

Build the project:

```
cabal build
```

## Run

To run an example:

```
cabal run <exampleNN>
```

Where `NN` is the example number.

## Examples

The examples presented here are meant to demonstrate good practices in video
game development using modern hardware. Notably, because practically every
device now has a GPU, programming in software rendering is no longer relevant
and will not be addressed.

### Example 01 - Show Window

The first example displays a window with blank surface.

The `main` function performs the bare minimum to display a window.

```haskell
main :: IO ()
main = do
  SDL.initializeAll
  window <- SDL.createWindow windowTitle SDL.defaultWindow
  renderer <- SDL.createRenderer window (-1) SDL.defaultRenderer
  loop renderer
  shutdown window
```

SDL must always be initialized first. In this example, all subsystems are
initialized, but it's also possible to initialize only some subsystems. In this
example, video would suffice:

```haskell
SDL.initialize [SDL.InitVideo]
```

The window and renderer are both created with default settings, to keep the
example simple. The default window is unresizable and has `800x600` resolution.
The default renderer is hardware accelerated, without v-sync, or texture render
target.

The renderer hides how graphics are drawn. It is cross platform and hardware
accelerated by default, thus depending on the operating system, it chooses
Direct3D, OpenGL, or OpenGL ES. It's intended for 2D games. If you wish to
create 3D games, instead of using the renderer, use an OpenGL or Vulkan
context.

In the main loop, each cycle renders a white surface, unless the user quits.

```haskell
loop :: SDL.Renderer -> IO ()
loop renderer = do
  events <- SDL.pollEvents
  let quit = SDL.QuitEvent `elem` map SDL.eventPayload events
  SDL.rendererDrawColor renderer SDL.$= white
  SDL.clear renderer
  SDL.present renderer
  unless quit $ loop renderer
```

It's worth noting that all events are polled in each cycle, rather than just
one. Each cycle does not always generate a single event, so all events are
pooled per cycle. Extra events would otherwise remain in the queue and never be
processed, resulting in a memory leak.

The following line checks for the presence a quit event. A quit event is
usually generated when the user closes the window, but it can also be generated
for other causes, such as catching a `SIGINT` or `SIGTERM`. The quit event is
described in further detail [here][sdl-quit-event].

Before drawing anything, the screen should be cleared. In SDL this means
filling the entire screen with a color. This example sets the drawing color to
white and uses the `clear` function to fill the screen. If the screen is not
cleared in each cycle, subsequent cycles will keep the previous drawings.

Because SDL rendering uses double buffering, rendering alone does not output
anything on the screen. The `present` function must be called after rendering.
Otherwise the rendering will only be exist in memory, but not on screen. To see
what happens, try commenting the `present` line.

Before exiting the application, all resources should be cleared:

```haskell
shutdown :: SDL.Window -> IO ()
shutdown window = do
  SDL.destroyWindow window
  SDL.quit
```

This sample demonstrated the fundamental code structure of a game. The
following example demonstrates how to use textures to display an image on the
screen.

## Example 02 - Show Image

Shows how to display an image. The drawing was made by myself.

The image is loaded in the main function.

```haskell
main :: IO ()
main = do
  ...
  texture <- loadTexture renderer "image.png"
  loop renderer texture
  shutdown window texture

loadTexture :: SDL.Renderer -> FilePath -> IO SDL.Texture
loadTexture renderer path = do
  image <- getDataFileName path >>= SDL.Image.load
  texture <- SDL.createTextureFromSurface renderer image
  SDL.freeSurface image
  return texture
```

We use [sdl2-image][sdl2-image], which supports `png` and other formats,
because SDL2 only supports `bmp`. The result of loading an image is a surface.
Surfaces are intended for software rendering, but we want hardware
acceleration, therefore we transform the surface to a texture, and free the
surface, since it's not needed anymore.

Textures are stored in GPU VRAM and have a power of two in size. If your image
is not a power of two, the GPU will pad it on runtime, resulting in a small
delay that, when added together, may impact performance. Some older GPUs don't
even accept non-power of two textures, while some are further restrictive, only
accepting squared textures. Most devices support at least `2048x2048` textures,
which may not be enough for you game (e.g., if it's 4K) or may be too large
(e.g., if it's a low-resolution game). The amount GPU VRAM is also limited.
Most devices have at least 2GB of VRAM (including mobile phones), but can have
up to as 16GB or more, and the trend is to have more in the future.  For small
2D games, none of this is a big concern, nonetheless, loading fewer textures
results in fewer system calls, so consider having many sprites in large
textures, and if you want to support a wide range of devices, don't use too
much VRAM.

To render we copy the entire texture to the renderer.

```haskell
loop :: SDL.Renderer -> SDL.Texture -> IO ()
loop renderer texture = do
  ...
  SDL.copy renderer texture Nothing Nothing
  ...
  unless quit $ loop renderer texture
```

The last two arguments are rectangles for the texture's source and the
renderer's destination. This is useful for clipping, but for now we just render
the full texture, in the entire renderer, which results in the texture
stretching over the entire screen.

Finally we destroy the texture on exit:

```haskell
shutdown :: SDL.Window -> SDL.Texture -> IO ()
shutdown window texture = do
  SDL.destroyTexture texture
  ...
```

Sprites are used in the majority of 2D games, so we must understand how to clip
sprites from a texture. Because clipping is done using rectangles, the
following example involves geometry.

## Example 03 - Geometry

Shows how to draw rectangles, lines and points.

The resizable option was enabled to show how geometry sizes can be adjusted
based on the size of the window.

```haskell
main :: IO ()
main = do
  ...
  window <- SDL.createWindow windowTitle SDL.defaultWindow {SDL.windowResizable = True}
  ...
```

The geometry objects are drawn using a `render` call in the main loop.

```haskell
loop :: SDL.Window -> SDL.Renderer -> IO ()
loop window renderer = do
  ...
  render window renderer
  ...
```

Drawing functions were created per geometry, to make the code easier to
understand.

```haskell
-- | A 2D position
type Position = Point V2 CInt

-- | A 2D size
type Size = V2 CInt

-- | Renders multiple points
renderPoints :: SDL.Renderer -> V.Vector Position -> Color -> IO ()
renderPoints renderer positions color = do
  SDL.rendererDrawColor renderer $= color
  SDL.drawPoints renderer positions

-- | Renders a line
renderLine :: SDL.Renderer -> Position -> Position -> Color -> IO ()
renderLine renderer start end color = do
  SDL.rendererDrawColor renderer $= color
  SDL.drawLine renderer start end

-- | Renders a rectangle outline
renderRect :: SDL.Renderer -> Position -> Size -> Color -> IO ()
renderRect renderer position size color = do
  let rectangle = Just $ SDL.Rectangle position size
  SDL.rendererDrawColor renderer $= color
  SDL.drawRect renderer rectangle

-- | Renders a filled rectangle
renderFillRect :: SDL.Renderer -> Position -> Size -> Color -> IO ()
renderFillRect renderer position size color = do
  let rectangle = Just $ SDL.Rectangle position size
  SDL.rendererDrawColor renderer $= color
  SDL.fillRect renderer rectangle
```

The pattern shared by these functions is to set a rendering color and then use
the appropriate drawing function. Because drawing a single point is
uninteresting or unnoticeable, we use the multiple points function instead.
There are also functions for multiple lines and rectangle, but we will use them
in this example. The multiple lines function draws connected lines, so it could
be handy. You may be wondering why `drawRect` and `fillRect` take an optional
rectangle. `Nothing` draws the whole rendering context, whereas `Just r` draws
the provided rectangle.

The `render` function draws a filled red rectangle, an outline of a blue
rectangle, a vertical green line and an horizontal dashed line.

```haskell
-- | Renders a set of primitives
render :: SDL.Window -> SDL.Renderer -> IO ()
render window renderer = do
  windowSize <- SDL.get $ SDL.windowSize window
  let windowWidth = windowSize ^._x
  let windowHeight = windowSize ^._y
  let windowCenter = fmap (`div` 2) windowSize

  -- Renders a red filled rectangle in the center of the screen
  let rectSize = fmap (`div` 6) windowSize
  let rectHalfSize = fmap (`div` 2) rectSize
  let rectPosition = P $ windowCenter - rectHalfSize
  renderFillRect renderer rectPosition rectSize red

  -- Renders a blue rectangle outline in the center of the screen
  renderRect renderer rectPosition rectSize blue

  -- Renders a vertical green line in the center of the screen
  let lineStart = P $ V2 (windowWidth `div` 2) 0
  let lineEnd = P $ V2 (windowWidth `div` 2) windowHeight
  renderLine renderer lineStart lineEnd green

  -- Renders an horizontal dashed black line in the center of the screen
  let pointSpacing = 5
  let pointYPos = windowHeight `div` 2
  let pointXPos = [0, pointSpacing .. windowWidth]
  let pointsPos = V.fromList $ map (P . flip V2 pointYPos) pointXPos
  renderPoints renderer pointsPos black
```

The placements and sizes of the geometries are determined by the size of the
window. Both rectangles are one-sixth the size of the window, and the lines are
centered. The dashed line a series of points of fixed spacing, in this case 5
pixels.

Now that we understand how geometry works, we can clip parts of a texture. This
will be for animation, which we show next.

## Example 04 - Animation

Shows a soldier's walking down animation.

2D games typically use one or more sprite sheets, which combine several sprites
into a single big texture. This is due to the fact that loading a single image
into VRAM is faster than loading many images. As previously stated, textures
should have a power of two size, for efficiency and compatibility. Otherwise
some GPU will pad the texture, cause glitches, or even refuse to load.

The example uses a [NES styled soldier sprite sheet][soldier-spritesheet],
which provides walking animations for various characters. Because the sprites
are of the same size (32x32), clipping each one is simple. The image was
resized to 512x512 pixels and given a transparent background.

A transparent background isn't required because SDL supports color keying,
which changes one color to transparent, but that would only save a few bytes in
exchange for losing one color and a bit of performance. The image would have to
be transformed with alpha channels on the fly, so there isn't much reason to
avoid images with alpha channels already included.

We load the sprite sheet before the game loop:

```haskell
main = do
  ...
  texture <- loadTexture renderer "soldier.png"
  loop window renderer texture
  shutdown window texture
```

In 2D games, assets, including sprites, are often loaded before each level, but
since we don't have levels, we just load everything at the start.

The game loop renders the animation of a character walking down:

```haskell
-- | A sprite rectangle based on a 32x32 grid
spriteRect :: CInt -> CInt -> SDL.Rectangle CInt
spriteRect x y = SDL.Rectangle position size
  where
    position = P $ size * V2 x y
    size = V2 32 32

-- | Player walking down animation sprites
walkDownSprites :: V.Vector (SDL.Rectangle CInt)
walkDownSprites = V.fromList $ map (`spriteRect` 0) [0, 2]

-- | Player walking down animation speed in seconds per frame
walkDownSpeed :: Double
walkDownSpeed = 0.25

-- | Renders sprites
renderSprites :: SDL.Renderer -> SDL.Texture -> Double -> IO ()
renderSprites renderer texture time = do
  let index = floor (time / walkDownSpeed) `mod` length walkDownSprites
  let source = Just $ walkDownSprites V.! index
  let destination = Just $ spriteRect 0 0
  SDL.copy renderer texture source destination

-- | The main loop
loop :: SDL.Window -> SDL.Renderer -> SDL.Texture -> IO ()
loop window renderer texture = do
  time <- SDL.time
  ...
  renderSprites renderer texture time
  ...
  unless quit $ loop window renderer texture
```

The `spriteRect` function is a convenience function that returns a 32x32
rectangle given x and y coordinates. For example, the `x = 2` and `y = 1`
coordinates return a rectangle of size `32x32` in the `(64, 32)` position.
It's a straightforward method for obtaining sprites in a `32x32` grid.

The `walkDownSprites` gets the sprites that are part of the walking down
animation. The `walkDownSpeed` defines the speed of the animation in seconds
per frame. A quarter of a second per frame gives a good result.

The `renderSprites` function renders the soldier walking down. Printing a
sprite on screen is easy. The `copy` function allows to take a sprite from the
texture, with the `source` argument, and put on the renderer, with
`destination` argument, so we put a sprite on the top left corner of the
screen. The tricky part is knowing which sprite to show. We can't simply pick a
sprite based on frames per second, because some machines are faster than
others. Instead we pick a sprite based on the time passed since the beginning
of the game, with the `time` function. First we calculate how many walking
frames should have been shown, with `time / walkDownSprites`, and modulus with
the total number of sprites in the animation. This gives us `0` or `1`,
depending on how many quarters of a second have passed, and we use that as the
index of the walking animation.

The next step is moving our character.

## Example 05 - Keyboard Input

Shows how to handle keyboard input.

This is example is similar to a gamepad test application but with the keyboard.
The arrow keys are equivalent to the d-pad and `Z` and `X` to the `A` and `B`
buttons. Squares are used to represent each key press.

### General Information

SDL represents keys in two ways: scancodes and keycodes. A scancode represents
the physical location of a key, while a keycode represents the layout location.
Scancode values are based on the US QWERTY layout. In a `QUERTZ` layout, if the
`Z` key was pressed, the scancode would be `Y`, and the keycode, `Z`.

You might be wondering why anyone want to use scancodes instead of keycodes.
Scancodes are useful for de facto standard keys, like `WASD`, or arrow keys.
If scancodes are used, it doesn't matter which layout is configured, `WASD`
will always be the top left corner keys. In layouts other than `QUERTY` (e.g.,
`AZERTY`, `QUERTZ`) the user would have to manually setup the keycodes to match
`WASD` scancodes, which is not very convenient.

In this example only scancodes are used because they're more convenient, but
using keycodes isn't much different. The only difference is an extra step to
convert scancodes to keycodes.

There are also two ways to handle keyboard input: state or events. The keyboard
state tells if a key is being pressed, and events tell whether a key was
pressed or released. Keyboard state is suitable for any action that continues
as long as a key is being pressed, like moving, or firing an automatic rifle.
Keyboard events are suitable for text input or actions that occur only once,
like opening a door, or firing a revolver. In this example the d-pad handling
is done with keyboard state and buttons with events.

One particularity of keyboard events is that repetition is supported. If a key
is kept pressed, it will repeat after a while. The repeat delay and rate are OS
configurations. This is useful for typing, but not much else. It's very easy to
overlook repetition since it's enabled by default. Thankfully it's also very
easy to ignore repetition.

Keyboard state is detailed next.

### Keyboard State

Handling keyboard state is straight-forward. It's just a matter of checking if
current key state is active or not, and compute based on it.

Keyboard state will be used to represent 8 directions. This will be shown as a
red square in the middle of the screen that moves when arrows keys are being
are pressed.

We start by defining what a direction is:

```haskell
-- Two dimensional directions
data Direction
  = -- | ^ Up
    U
  | -- | ^ Down
    D
  | -- | ^ Left
    L
  | -- | ^ Right
    R
  deriving (Show, Eq)
```

We also need a mapping between directions and scancodes.

```haskell
-- | direction to scancode map
directionMap :: M.Map Direction SDL.Scancode
directionMap =
  M.fromList
    [ (U, SDL.ScancodeUp),
      (D, SDL.ScancodeDown),
      (L, SDL.ScancodeLeft),
      (R, SDL.ScancodeRight)
    ]
```

In the main loop we get the keyboard state, directions based on the keyboard
state and finally render the directions:

```haskell
loop :: SDL.Window -> SDL.Renderer -> M.Map Action Bool -> IO ()
loop w r ss = do
  ...
  directions <- M.keys . (`M.filter` directionMap) <$> SDL.getKeyboardState
  ...
  renderDirection w r directions
```

The `getKeyboardState` function gets a snapshot of the current state of the
keyboard, and returns a `(Snapshot -> Bool)` function, which returns `True` if
given key is being pressed, or `False` otherwise. With that function it's
simple to filter only the keys being pressed. The result of the filtering is a
`Map Direction Scancode`, and since we only need the directions, we get the map
keys.

The rendering function draws a red square in the middle of the screen, that
moves to the given directions:

```haskell
-- | Renders the current direction
renderDirection :: SDL.Window -> SDL.Renderer -> [Direction] -> IO ()
renderDirection w r ds = do
  windowSize <- SDL.get $ SDL.windowSize w
  let windowCenter = fmap (`div` 2) windowSize
  let centerPos = windowCenter - fmap (`div` 2) rectSize
  let rectPos = P $ foldr ((+) . move) centerPos ds
  renderRect r rectPos red
  where
    move d =
      rectSize * case d of
        U -> V2 0 (-1)
        D -> V2 0 1
        L -> V2 (-1) 0
        R -> V2 1 0
```

Most of the code is just calculations to see were to position the square, given
the directions. The position is calculated by adding all directions. This means
that if left and up are pressed simultaneously, the square will move upper
left. This is how we get 8 directions.

Keyboard events are shown next.

## Buttons

Keyboard events are generated when a key is pressed or released. Perhaps not so
immediately obvious is that drawing something on screen, when a key is pressed,
won't amount to much. The drawing would only be shown in one frame, when the
key press is detected. To show something on screen this example does has
similar behavior as keyboard state, but with events. That is, if `Z` or `X` are
kept pressed, a red or blue square are shown, and hidden when the respective
key is released. For that the buttons need visibility state, so we know whether
the buttons should be kept visible or not, even without keyboard events in that
cycle.

We start by defining what a button is:

```haskell
-- | A game action
data Button = A | B deriving (Show, Eq, Ord)
```

It's a two buttons gamepad, with `A` and `B` buttons.

We also need a scancode to button mapping.

```haskell
-- | Scancode to action map
buttonMap :: M.Map SDL.Scancode Button
buttonMap =
  M.fromList
    [ (SDL.ScancodeZ, A),
      (SDL.ScancodeX, B)
    ]
```

The reason why scancodes are keys and buttons values, is because `lookup` will
be used instead of `filter`, so it's just more convenient this way.

As mentioned before, the buttons need state to figure whether should be visible
or not in each frame, so we have a default buttons functions:

```haskell
type Visible = Bool

-- | Default buttons and visibility
defaultButtons :: M.Map Button Visible
defaultButtons = M.fromList [(A, False), (B, False)]
```

By default buttons are invisible. The game loop needs to be started with
default buttons:

```haskell
main = do
  ...
  loop window renderer defaultButtons
```

The main loop checks keyboard events that are mapped to buttons and calculates
whether the buttons should be visible or not:

```haskell
loop :: SDL.Window -> SDL.Renderer -> M.Map Button Visible -> IO ()
loop w r bs = do
  ...
  let keyboardEvents = mapMaybe keyboardEventData events
  let buttonsMotion = mapMaybe buttonMotion keyboardEvents
  let bs' = foldr (\x xs -> M.adjust (const $ snd x == SDL.Pressed) (fst x) xs) bs buttonsMotion
  ...
  renderButtons r bs'
  ...
  unless quit $ loop w r bs'
```

The first line gets all keyboard events. Checking the type is enough to know
which events are keyboard events:

```haskell
keyboardEventData :: SDL.EventPayload -> Maybe SDL.KeyboardEventData
keyboardEventData p = case p of
  SDL.KeyboardEvent e -> Just e
  _ -> Nothing
```

The second line gets matching buttons and the respective input motion (press or
release). For that we need to convert keyboard events to a pair of button and
input motion:

```haskell
buttonMotion :: SDL.KeyboardEventData -> Maybe (Button, SDL.InputMotion)
buttonMotion e = case M.lookup (scancode e) buttonMap of
  Just b -> if not $ repeat' e then Just (b, keyMotion e) else Nothing
  Nothing -> Nothing
  where
    scancode = SDL.keysymScancode . SDL.keyboardEventKeysym
    keyMotion = SDL.keyboardEventKeyMotion
    repeat' = SDL.keyboardEventRepeat
```

Keyboard event data allows to get the symcode, key motion and whether it was a
repeated input or not. Ignoring key repetition in this example, doesn't change
anything, but it's still ignored just to show how to avoid unnecessary
repetition.

The button state must be updated, so the button visibility is adjusted
accordingly. If the button was pressed the button must be visible, otherwise
must be hidden. If no key was pressed or released, the visibility remains the
same as the previous cycle.

Finally the buttons are rendered:

```haskell
renderButtons :: SDL.Renderer -> M.Map Button Visible -> IO ()
renderButtons r as = do
  when (A `M.lookup` as == Just True) renderA
  when (B `M.lookup` as == Just True) renderB
  where
    renderA = do
      let pos = P $ V2 0 0
      renderRect r pos blue
    renderB = do
      let pos = P $ rectSize * V2 1 0
      renderRect r pos green
```

The rendering function draws a red or blue square, depending on which key was
pressed, on the top left corner of the screen.

[sdl2-library]: https://hackage.haskell.org/package/sdl2
[sdl2-image]: https://hackage.haskell.org/package/sdl2-image
[sdl-quit-event]: https://wiki.libsdl.org/SDL_EventType#sdl_quit
[soldier-spritesheet]:[https://opengameart.org/content/more-nes-style-rpg-characters] 
