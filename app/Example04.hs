{-# LANGUAGE OverloadedStrings #-}

module Main where

import Color
import Control.Monad (unless)
import qualified Data.Text as T
import qualified Data.Vector as V
import Foreign.C.Types (CInt)
import Paths_sdl2_examples (getDataFileName)
import SDL (($=))
import qualified SDL
import qualified SDL.Image
import SDL.Vect (Point (P), V2 (..))

main :: IO ()
main = do
  SDL.initializeAll
  window <- SDL.createWindow windowTitle SDL.defaultWindow
  renderer <- SDL.createRenderer window (-1) SDL.defaultRenderer
  texture <- loadTexture renderer "soldier.png"
  loop window renderer texture
  shutdown window texture

-- | The window title
windowTitle :: T.Text
windowTitle = "Example 04 - Animation"

-- | Loads a texture
loadTexture :: SDL.Renderer -> FilePath -> IO SDL.Texture
loadTexture renderer filePath = do
  surface <- getDataFileName filePath >>= SDL.Image.load
  texture <- SDL.createTextureFromSurface renderer surface
  SDL.freeSurface surface
  return texture

-- | A sprite rectangle based on a 32x32 grid
spriteRect :: CInt -> CInt -> SDL.Rectangle CInt
spriteRect x y = SDL.Rectangle position size
  where
    position = P $ size * V2 x y
    size = V2 32 32

-- | Player walking down animation sprites
walkDownSprites :: V.Vector (SDL.Rectangle CInt)
walkDownSprites = V.fromList $ map (`spriteRect` 0) [0, 2]

-- | Player walking down animation speed in seconds per frame
walkDownSpeed :: Double
walkDownSpeed = 0.25

-- | Renders sprites
renderSprites :: SDL.Renderer -> SDL.Texture -> Double -> IO ()
renderSprites renderer texture time = do
  let index = floor (time / walkDownSpeed) `mod` length walkDownSprites
  let source = Just $ walkDownSprites V.! index
  let destination = Just $ spriteRect 0 0
  SDL.copy renderer texture source destination

-- | The main loop
loop :: SDL.Window -> SDL.Renderer -> SDL.Texture -> IO ()
loop window renderer texture = do
  time <- SDL.time
  events <- SDL.pollEvents
  let quit = SDL.QuitEvent `elem` map SDL.eventPayload events
  SDL.rendererDrawColor renderer SDL.$= white
  SDL.clear renderer
  renderSprites renderer texture time
  SDL.present renderer
  unless quit $ loop window renderer texture

-- | Frees any resources
shutdown :: SDL.Window -> SDL.Texture -> IO ()
shutdown window texture = do
  SDL.destroyTexture texture
  SDL.destroyWindow window
  SDL.quit
