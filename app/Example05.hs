{-# LANGUAGE OverloadedStrings #-}

module Main where

import Color (Color, blue, green, red, white)
import Control.Monad (unless, when)
import qualified Data.Map as M
import Data.Maybe (mapMaybe)
import qualified Data.Text as T
import Foreign.C.Types (CInt)
import SDL (($=))
import qualified SDL
import SDL.Vect (Point (P), V2 (..))

main :: IO ()
main = do
  SDL.initializeAll
  window <- SDL.createWindow windowTitle SDL.defaultWindow
  renderer <- SDL.createRenderer window (-1) SDL.defaultRenderer
  loop window renderer defaultButtons
  shutdown window

-- | The window title
windowTitle :: T.Text
windowTitle = "Example 05 - Keyboard Input"

-- Two dimensional directions
data Direction
  = -- | ^ Up
    U
  | -- | ^ Down
    D
  | -- | ^ Left
    L
  | -- | ^ Right
    R
  deriving (Show, Eq, Ord)

-- | direction to scancode map
directionMap :: M.Map Direction SDL.Scancode
directionMap =
  M.fromList
    [ (U, SDL.ScancodeUp),
      (D, SDL.ScancodeDown),
      (L, SDL.ScancodeLeft),
      (R, SDL.ScancodeRight)
    ]

-- | A game action
data Button = A | B deriving (Show, Eq, Ord)

type Visible = Bool

-- | Default buttons and visibility
defaultButtons :: M.Map Button Visible
defaultButtons = M.fromList [(A, False), (B, False)]

-- | Rectangle size
rectSize :: V2 CInt
rectSize = V2 100 100

-- | Renders a rectangle
renderRect :: SDL.Renderer -> Point V2 CInt -> Color -> IO ()
renderRect r p c = do
  let rect = SDL.Rectangle p rectSize
  SDL.rendererDrawColor r $= c
  SDL.fillRect r (Just rect)

-- | Renders the current direction
renderDirection :: SDL.Window -> SDL.Renderer -> [Direction] -> IO ()
renderDirection w r ds = do
  windowSize <- SDL.get $ SDL.windowSize w
  let windowCenter = fmap (`div` 2) windowSize
  let centerPos = windowCenter - fmap (`div` 2) rectSize
  let rectPos = P $ foldr ((+) . move) centerPos ds
  renderRect r rectPos red
  where
    move d =
      rectSize * case d of
        U -> V2 0 (-1)
        D -> V2 0 1
        L -> V2 (-1) 0
        R -> V2 1 0

-- | Scancode to action map
buttonMap :: M.Map SDL.Scancode Button
buttonMap =
  M.fromList
    [ (SDL.ScancodeZ, A),
      (SDL.ScancodeX, B)
    ]

-- | Renders buttons
renderButtons :: SDL.Renderer -> M.Map Button Visible -> IO ()
renderButtons r as = do
  when (A `M.lookup` as == Just True) renderA
  when (B `M.lookup` as == Just True) renderB
  where
    renderA = do
      let pos = P $ V2 0 0
      renderRect r pos blue
    renderB = do
      let pos = P $ rectSize * V2 1 0
      renderRect r pos green

-- | Returns the keyboard event data if the event is from a keyboard
keyboardEventData :: SDL.EventPayload -> Maybe SDL.KeyboardEventData
keyboardEventData p = case p of
  SDL.KeyboardEvent e -> Just e
  _ -> Nothing

-- | Returns the button motion
buttonMotion :: SDL.KeyboardEventData -> Maybe (Button, SDL.InputMotion)
buttonMotion e = case M.lookup (scancode e) buttonMap of
  Just b -> if not $ repeat' e then Just (b, keyMotion e) else Nothing
  Nothing -> Nothing
  where
    scancode = SDL.keysymScancode . SDL.keyboardEventKeysym
    keyMotion = SDL.keyboardEventKeyMotion
    repeat' = SDL.keyboardEventRepeat

-- | The main loop
loop :: SDL.Window -> SDL.Renderer -> M.Map Button Visible -> IO ()
loop w r bs = do
  events <- map SDL.eventPayload <$> SDL.pollEvents
  let quit = SDL.QuitEvent `elem` events
  directions <- M.keys . (`M.filter` directionMap) <$> SDL.getKeyboardState
  let keyboardEvents = mapMaybe keyboardEventData events
  let buttonsMotion = mapMaybe buttonMotion keyboardEvents
  let bs' = foldr (\x xs -> M.adjust (const $ snd x == SDL.Pressed) (fst x) xs) bs buttonsMotion
  SDL.rendererDrawColor r $= white
  SDL.clear r
  renderDirection w r directions
  renderButtons r bs'
  SDL.present r
  unless quit $ loop w r bs'

-- | Frees any resources
shutdown :: SDL.Window -> IO ()
shutdown window = do
  SDL.destroyWindow window
  SDL.quit
