{
  description = "virtual environments";

  inputs.devshell.url = "github:numtide/devshell";
  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.nix-filter.url = "github:numtide/nix-filter";

  outputs = { self, flake-utils, nix-filter, devshell, nixpkgs }:
    with nixpkgs.lib;
    with flake-utils.lib;
    eachSystem [ "x86_64-linux" ] (system:
      let
        ghc = "ghc921";
        version =
          "${substring 0 8 self.lastModifiedDate}.${self.shortRev or "dirty"}";
        build = f: h: p:
          with f.haskell.lib;
          dontHaddock (dontCheck (disableLibraryProfiling p));
        hsPkgs = f:
          f.haskell.packages.${ghc}.override {
            overrides = hf: _: {
              linear = build f hf hf.linear_1_21_8;
              sdl2 = build f hf hf.sdl2_2_5_3_1;
              sdl2-image = build f hf hf.sdl2-image_2_1_0_0;
            };
          };
        overlay = f: _:
          with (hsPkgs f); {
            sdl2-examples = build f (hsPkgs f)
              ((callCabal2nix "sdl2-examples" self { }).overrideAttrs
                (o: { version = "${o.version}-${version}"; }));
          };
        pkgs = import nixpkgs {
          inherit system;
          overlays = [ devshell.overlay overlay ];
        };
      in {
        inherit overlay;
        packages = { inherit (pkgs) sdl2-examples; };
        devShell = pkgs.devshell.mkShell {
          name = "SDL2-EXAMPLES";
          imports = [ (pkgs.devshell.importTOML ./devshell.toml) ];
          packages = with pkgs;
            with (hsPkgs pkgs);
            [ (ghcWithPackages (_: [ sdl2-examples ])) ];
        };
      });
}
