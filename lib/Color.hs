module Color where

import Data.Word
import SDL.Vect

type Color = V4 Word8

white :: Color
white = V4 maxBound maxBound maxBound maxBound :: Color

black :: Color
black = V4 minBound minBound minBound maxBound :: Color

red :: Color
red = V4 maxBound minBound minBound maxBound :: Color

green :: Color
green = V4 minBound maxBound minBound maxBound :: Color

blue :: Color
blue = V4 minBound minBound maxBound maxBound :: Color
